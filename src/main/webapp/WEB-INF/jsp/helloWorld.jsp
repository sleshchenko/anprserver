<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Spring MVC Multiple File Upload</title>
    <script
            src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#addMoreFile').click(function () {
                $('#uploadTable').append(
                        '<tr><td><input type="file" name="multiUploadedFileList[' + $('#uploadTable tr').length + ']" />' +
                        '</td></tr>');
            });

        });
    </script>
</head>
<body>
<h1>${message}</h1>
<form:form method="post" action="multiFileUpload.web"
           commandName="multiFileUpload" enctype="multipart/form-data">

    <h3>Click Add More Files button to add files</h3>
    <input id="addMoreFile" type="button" value="Add More Files"/><br/><br/>
    <table id="uploadTable" border="2">
        <tr>
            <td><input name="multiUploadedFileList[0]" type="file"/></td>
        </tr>
    </table>
    <br/>
    <input type="submit" value="Upload Files"/>
</form:form>
</body>
</html>