package cdu.edu.ua.anpr.server;

import cdu.edu.ua.anpr.imageanalysis.Plate;
import cdu.edu.ua.anpr.intelligence.Intelligence;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

@Controller
public class UploadController {
    @RequestMapping(value = "/helloWorld.web", method = RequestMethod.GET)
    public String printWelcome(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
        model.addAttribute("message", "Spring MVC Multi File upload");
        return "helloWorld";

    }

    @RequestMapping(value = "/multiFileUpload.web", method = RequestMethod.POST)
    public String save(@ModelAttribute("multiFileUpload") MultiFileUpload multiFileUpload,
                       BindingResult bindingResult,
                       Model model) throws IOException {
        List<MultipartFile> files = multiFileUpload.getMultiUploadedFileList();
        StringBuffer uploadedfile = new StringBuffer("Below files uploaded successfully");

        if (files.isEmpty()) {
            bindingResult.reject("noFile", "Please select file to upload");
            return "helloWorld";
        }

        for (MultipartFile multipartFile : files) {
            String fileName = multipartFile.getOriginalFilename();
            String extensionOfFileName = fileName.substring(fileName.indexOf(".") + 1, fileName.length());
            if (extensionOfFileName.equalsIgnoreCase("png") || extensionOfFileName.equalsIgnoreCase("jpg")) {
                BufferedImage imBuff = ImageIO.read(multipartFile.getInputStream());
                Plate plate = new Plate(imBuff);
                try {
                    Intelligence intelligence = new Intelligence();
                    String recognize = intelligence.recognize(plate, true, false);//TODO FIx NPE When need report == false
                    uploadedfile.append("<h4>").append(fileName).append("->").append(recognize).append("</h4>");
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                } catch (SAXException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Unknown file extension" + extensionOfFileName);
            }
        }

        model.addAttribute("message", uploadedfile);
        return "Upload";
    }

}