package cdu.edu.ua.anpr.server;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class MultiFileUpload {

    private List<MultipartFile> multiUploadedFileList;

    public List<MultipartFile> getMultiUploadedFileList() {
        return multiUploadedFileList;
    }

    public void setMultiUploadedFileList(List<MultipartFile> multiUploadedFileList) {
        this.multiUploadedFileList = multiUploadedFileList;
    }
}
